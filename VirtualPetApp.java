import java.util.Scanner;
public class VirtualPetApp
{
	public static void main(String[] args)
	{
		Scanner reader = new Scanner(System.in);
		Lion[] pride = new Lion[1];
		//Making an array of objects using the class file made.
		for(int i = 0; i < pride.length; i++)
		//Does a loop that makes an animal object at each index.
		//After that it asks the user questions about the fields and take their user input.
		{
			System.out.println("How much does your animal weigh in pounds?");
			int weight = Integer.parseInt(reader.nextLine());
			
			System.out.println("How many teeth does your animal have?");
			int numOfTeeth = Integer.parseInt(reader.nextLine());
			
			System.out.println("Which family does your animal belong to?");
			String family = (reader.nextLine());
			
			pride[i]= new Lion(weight, numOfTeeth, family);
		}
		System.out.println(pride[0].getWeight());
		System.out.println(pride[0].getNumOfTeeth());
		System.out.println(pride[0].getFamily());
		
		System.out.println("Change your lion's family");
		pride[0].setFamily(reader.nextLine());
		
		System.out.println(pride[0].getWeight());
		System.out.println(pride[0].getNumOfTeeth());
		System.out.println(pride[0].getFamily());
	}
}