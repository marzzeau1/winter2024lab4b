public class Lion
{
	private int weight;
	private int numOfTeeth;
	private String family;
	
	//constructor
	public Lion(int weight, int numOfTeeth, String family){
		this.weight = weight;
		this.numOfTeeth = numOfTeeth;
		this.family = family;
	}
	//set method
	public void setFamily(String family){
		this.family = family;
	}
	//get methods
	public int getWeight(){
		return this.weight;
	}
	public int getNumOfTeeth(){
		return this.numOfTeeth;
	}
	public String getFamily(){
		return this.family;
	}
	//isObese
	public void isObese()
	//checks the weight and prints back answers based on the amount.
	{
		if(this.weight >=450)
		{
			System.out.println("Wow that lion is quite obese!");
		}
		else if(this.weight < 450 && this.weight >= 390)
		{
			System.out.println("Wow that lion is quite the avergage and healthy weight!");
		}
		else
		{
			System.out.println("Wow that's one skinny lion!");
		}
	}
	//biteDamgage
	public void biteDamage()
	//Checks the number of teeth and prints back answers based on the amount.
	{
		if(this.numOfTeeth >=24)
		{
			System.out.println("Wow that bite is going to hurt!");
		}
		else
		{
			System.out.println("That bite is embarrasing. Get ur teeth up bro.");
		}
	}	
}